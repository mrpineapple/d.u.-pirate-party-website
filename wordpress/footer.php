<div class="section" id="footer">
  <div class="w-container link-container">
    <div class="w-row">
      <div class="w-col w-col-4 left-footer-col">
        <div class="footer-text">Pirate Party International
          <br>Pirate Party Ireland
          <br>Electronic Frontier Foundation
          <br>Digital Rights Ireland
          <br>Creative Commons</div>
      </div>
      <div class="w-col w-col-4 footer-nav-bar" style="text-align:center;margin-top:30px">
        <img src="images/facebook.jpg" width="50" alt="facebook.png">&nbsp;
        <img src="images/twitter.jpg" width="50" alt="twitter_bird3_webtreatsetc.png">&nbsp;
        <img src="images/rss.jpg" width="50" alt="rss_1.png">
      </div>
      <div class="w-col w-col-4">
        <img class="cc-license" src="images/88x31.png" alt="88x31.png">
        <div class="cc-text">This site is licensed under a <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License.</a></div>
      </div>
    </div>
  </div>
</div>