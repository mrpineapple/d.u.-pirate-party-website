$(document).ready(function(){
	$(".newsitem").hover(
		function(){
			var container = $(this).next();
			var text = $(this).next().find(".snippet-text")
			text.slideDown('fast');
			container.css("height","auto");
			container.css("max-height","none");
		},
		function(){
			var container = $(this).next();
			var text = $(this).next().find(".snippet-text")
			text.slideUp('fast',function(){
				container.animate({height:"37px"},200);
			});
//			container.css("max-height","none");
		}
	)
});