<!--         <div class="w-col w-col-4" style="width:25%">
              <h3>Archives</h3>
              <p class="archive-date">October 2013</p>
              <p class="archive-date">September 2013</p>
              <p class="archive-date">August 2013</p>
        </div> -->
<!-- begin sidebar -->
<ul>
  <?php wp_list_pages('exclude=5'); ?>
  <li id="categories"><?php _e('Categories:'); ?>
	<ul>
	<?php wp_list_cats(); ?>
	</ul>
 </li>
 <li id="archives"><?php _e('Archives:'); ?>
 	<ul>
	 <?php wp_get_archives('type=monthly'); ?>
 	</ul>
 </li>
 <li id="search">
   <label for="s"><?php _e('Search:'); ?></label>	
   <form id="searchform" method="get" action="<?php bloginfo('home'); ?>">
	<div>
		<input type="text" name="s" id="s" size="15" /><br />
		<input type="submit" value="<?php _e('Search'); ?>" />
	</div>
	</form>
 </li>
<!--<?php get_links_list(); ?> -->
 <li id="meta"><?php _e('Meta:'); ?>
 	<ul>
		<?php wp_register(); ?>
		<li><?php wp_loginout(); ?></li>
		<li><a href="feed:<?php bloginfo('rss2_url'); ?>" title="<?php _e('Syndicate this site using RSS'); ?>"><?php _e('<abbr title="Really Simple Syndication">RSS</abbr>'); ?></a></li>
		<li><a href="feed:<?php bloginfo('comments_rss2_url'); ?>" title="<?php _e('The latest comments to all posts in RSS'); ?>"><?php _e('Comments <abbr title="Really Simple Syndication">RSS</abbr>'); ?></a></li>
		<?php wp_meta(); ?>
	</ul>
 </li>

</ul>
<!-- end sidebar -->
